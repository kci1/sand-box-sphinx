.. raw:: html

   <div id="page">

.. raw:: html

   <div id="main" class="aui-page-panel">

.. raw:: html

   <div id="main-header">

.. raw:: html

   <div id="breadcrumb-section">

.. raw:: html

   </div>

.. rubric::  Rémi Paquette : Conf2readthedocs
   :name: title-heading
   :class: pagetitle

.. raw:: html

   </div>

.. raw:: html

   <div id="content" class="view">

.. raw:: html

   <div class="page-metadata">

Created by Rémi Paquette on déc. 23, 2019

.. raw:: html

   </div>

.. raw:: html

   <div id="main-content" class="wiki-content group">

| 

.. rubric:: Simple table
   :name: Conf2readthedocs-Simpletable

.. raw:: html

   <div class="table-wrap">

+------+------+------+
| h1   | h2   | h3   |
+======+======+======+
| a    | b    | c    |
+------+------+------+

.. raw:: html

   </div>

.. rubric:: Merged cell table
   :name: Conf2readthedocs-Mergedcelltable

.. raw:: html

   <div class="table-wrap">

h1
h2 merge h3
a
b
c
a
b merged c

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="footer" role="contentinfo">

.. raw:: html

   <div class="section footer-body">

